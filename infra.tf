terraform {
  backend "gcs" {
    bucket  = "companyh_foldera_projectdev_terraform_state"
    prefix  = "terraform/state"
  }
}


module "vpc" {
    source  = "git@gitlab.com:h-company-terraform-in-code-management/modules/management-services/vpc-provisioning-module.git?ref=v0.0.1"
    

    project_id   = "regal-module-307818"
    network_name = "foldera-projectdev-vpc"

    shared_vpc_host = false
}
